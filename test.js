$(document).ready(function () {
  var ePosDev = new epson.ePOSDevice();
  var printer = null;
  function connect() {
    //Connects to a device
    ePosDev.connect('192.168.0.38', '8008', callback_connect);
    console.log(ePosDev);
  }
  function callback_connect(resultConnect) {
    if ((resultConnect == 'OK') || (resultConnect == 'SSL_CONNECT_OK')) {
      //Retrieves the Printer object
      ePosDev.createDevice('local_printer', ePosDev.DEVICE_TYPE_PRINTER, {
        'crypto': false,
        'buffer': false
      }, callback_createDevice);
    }
    else {
      //Displays error messages
    }
  }
  function callback_createDevice(deviceObj, retcode) {
    printer = deviceObj;
    if (retcode == 'OK') {
      printer.timeout = 60000;
      // //Registers an event
      printer.onstatuschange = function (res) { alert(res.success); };
      printer.onbatterystatuschange = function (res) { alert(res.success); };
      print();
    } else {
      alert(retcode);
    }
  }
  function startMonitor() {
    //Starts the status monitoring process
    printer.startMonitor();
  }
  //Opens the printer cover
  function stopMonitor() {
    //Stops the status monitoring process
    printer.stopMonitor();
  }
  function disconnect() {
    //Discards the Printer object
    ePosDev.deleteDevice(printer, callback_deleteDevice);
  }
  function callback_deleteDevice(errorCode) {
    //Terminates connection with device
    ePosDev.disconnect();
  }
  
  function print() {
    //Create the printing data
    var text = $("#input").val() + '\n';
    printer.addText(text);
    printer.addPulse(printer.DRAWER_1, printer.PULSE_200);
    //Send the printing data
    printer.send();
  }

  $("#test").click(function () {
    connect();
  });
});